from random import randint

player_name = input("What is your name? ")

def game():
    guess_number = 1
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)
    print("Guess " + str(guess_number) + ": " + str(player_name) + " were you " + "born on " + str(month_number) + " / " + str(year_number) + "?")
    response = input("yes or no? ".lower())
    if response == "yes":
        print (" I knew it")
    else:
        print ("Drat! Let me try again")
        game()

game()
